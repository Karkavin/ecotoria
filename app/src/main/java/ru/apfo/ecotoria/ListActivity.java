package ru.apfo.ecotoria;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class ListActivity extends AppCompatActivity {

    private Typeface typeFaceLight;
    private Typeface typeFaceRegular;

    private TextView headetTextView;
//    private ImageButton backImageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        typeFaceLight = Typeface.createFromAsset(getAssets(),"fonts/HelveticaLight.ttf");
//        typeFaceRegular = Typeface.createFromAsset(getAssets(),"fonts/HelveticaRegular.ttf");
//
        headetTextView = (TextView) findViewById(R.id.tool_bar_header);
        headetTextView.setTypeface(typeFaceLight);
//
        headetTextView.setText("Тест");
//
//
//        backImageButton = (ImageButton) findViewById(R.id.tool_bar_back_button);
//        backImageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });


    }
}
